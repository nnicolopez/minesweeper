import { Component, EventEmitter, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { GameService } from '../services/game.service';

@Component({
  selector: 'app-minesweeper',
  templateUrl: './minesweeper.component.html',
  styleUrls: ['./minesweeper.component.scss']
})
export class MinesweeperComponent implements OnInit {
  settings: any = {};
  constructor(private route: ActivatedRoute,
              private router: Router,
              public game: GameService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.settings['rows'] = params['rows'];
      this.settings['columns'] = params['columns'];
      this.settings['difficulty'] = params['difficulty'];
      this.game.start(this.settings.columns, this.settings.rows, this.settings.difficulty);

    });
  }

  boxClicked(event: EventEmitter<any>, i: number, j: number) {
    const result = this.game.reveal(i, j);
    if (result) {
      if (result['lost']) {
        setTimeout(() => {
          alert('you lost');
          this.router.navigateByUrl('/settings');
        }, 300);
      } else if (result['won']) {
        setTimeout(() => {
          alert('you won');
          this.router.navigateByUrl('/settings');
        }, 300);
      }
    }
  }


}
