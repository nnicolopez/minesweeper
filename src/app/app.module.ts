import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MinesweeperComponent } from './minesweeper/minesweeper.component';
import { FormsModule } from '@angular/forms';
import { SettingsComponent } from './settings/settings.component';
import { GameService } from './services/game.service';
import { SquareDirective } from './square.directive';

@NgModule({
  declarations: [
    AppComponent,
    MinesweeperComponent,
    SettingsComponent,
    SquareDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    GameService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
