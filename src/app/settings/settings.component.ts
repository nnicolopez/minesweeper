import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  rows = 5;
  columns = 5;
  difficulty: number;

  difficulties = [
    {name: 'Easy', value: 1},
    {name: 'Medium', value: 3},
    {name: 'Hard', value: 5},
  ];

  constructor(
    private router: Router
  ) {}

  ngOnInit() {
    this.difficulty = this.difficulties.find(obj => obj.name === 'Medium')['value'];
  }

  onSubmit(form: FormControl) {
    const value = form.value;
    this.router.navigate(['/game', {rows: value.rows, columns: value.columns, difficulty: value.difficulty}]);
  }
}
