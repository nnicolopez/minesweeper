import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appSquare]'
})
export class SquareDirective implements OnInit {
  constructor(private el: ElementRef) {
  }

  ngOnInit() {
    this.el.nativeElement.style.height = this.el.nativeElement.clientWidth.toString() + 'px';
  }

}
