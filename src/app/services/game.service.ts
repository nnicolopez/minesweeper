import { Injectable } from '@angular/core';
import { Mine } from './mine.service';

@Injectable()
export class GameService {
  public space: Mine[][];
  public columns: number;
  public rows: number;
  private minesCount: number;
  private revealedCount: number;

  constructor() {
  }


  public start(rows: number, columns: number, dif: number = 3): void {
    this.columns = columns;
    this.rows = rows;
    this.space = [];
    const percentageOfMines = dif / 10;
    this.minesCount = Math.floor(this.columns * this.rows * percentageOfMines);
    this.revealedCount = 0;
    this.generateMineBoxes();
    this.generateMines();
    this.generateDanger();
  }

  private generateMineBoxes(): void {
    for (let i = 0; i < this.columns; i++) {
      this.space[i] = [];
      for (let j = 0; j < this.rows; j++) {
        this.space[i][j] = new Mine(i, j);
      }
    }
  }

  private generateMines(): void {
    let minesCount = this.minesCount;
    while (minesCount !== 0) {
      const randomI = Math.floor(Math.random() * this.columns);
      const randomJ = Math.floor(Math.random() * this.rows);
      if (!this.space[randomI][randomJ].getMine()) {
        this.space[randomI][randomJ].setMine(true);
        minesCount--;
      }
    }
  }

  private generateDanger(): void {
    for (let i = 0; i < this.columns; i++) {
      for (let j = 0; j < this.rows; j++) {
        let danger = 0;
        if (i !== 0) {
          if (this.space[i - 1][j].getMine()) {
            danger++;
          }
        }
        if (i !== this.columns - 1) {
          if (this.space[i + 1][j].getMine()) {
            danger++;
          }
        }
        if (j !== 0) {
          if (this.space[i][j - 1].getMine()) {
            danger++;
          }
        }
        if (j !== this.rows - 1) {
          if (this.space[i][j + 1].getMine()) {
            danger++;
          }
        }

        if (j !== this.rows - 1) {
          if (i !== this.columns - 1) {
            if (this.space[i + 1][j + 1].getMine()) {
              danger++;
            }
          }
        }
        if (j !== 0) {
          if (i !== 0) {
            if (this.space[i - 1][j - 1].getMine()) {
              danger++;
            }
          }
        }
        if (j !== this.rows - 1) {
          if (i !== 0) {
            if (this.space[i - 1][j + 1].getMine()) {
              danger++;
            }
          }
        }
        if (j !== 0) {
          if (i !== this.columns - 1) {
            if (this.space[i + 1][j - 1].getMine()) {
              danger++;
            }
          }
        }
        this.space[i][j].setDanger(danger);
      }
    }
  }

  private expand(i: number, j: number): void {
    if (!this.space[i][j].isRevealed()) {
      this.space[i][j].setRevealed(true);
      this.revealedCount++;
      if (this.space[i][j].danger === 0) {
        if (i + 1 < this.columns) {
          this.expand(i + 1, j);
        }
        if (j + 1 < this.rows) {
          this.expand(i, j + 1);
        }
        if (i - 1 >= 0) {
          this.expand(i - 1, j);
        }
        if (j - 1 >= 0) {
          this.expand(i, j - 1);
        }
      }
    }
  }


  public reveal(i: number, j: number) {
      if (this.space[i][j].getMine()) {
        this.space[i][j].setRevealed(true);
        return { lost: true };
      }
      this.expand(i, j);
      if (this.revealedCount === this.columns * this.rows - this.minesCount) {
        return {won: true};
      }
  }

}
